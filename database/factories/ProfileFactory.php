<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 10/26/2016
 * Time: 6:38 PM
 */
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'bio' => $faker->paragraph(5),
        'web' => $faker->url,
        'facebook' => 'http://facebook.com/'.$faker->unique()->firstName,
        'twitter' => 'http://twitter.com/'.$faker->unique()->firstName,
        'github' => 'http://github.com/'.$faker->unique()->firstName,
    ];
});
