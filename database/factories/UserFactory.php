<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 10/26/2016
 * Time: 6:38 PM
 */
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
