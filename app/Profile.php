<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable =[
        'user_id','bio','web','facebook','twitter','git'
    ];
    public function user(){
        return $this->belongsTo('App\user');
    }

}
