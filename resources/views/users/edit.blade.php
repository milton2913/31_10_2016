@extends('layouts.app')

@section('content')
    <div class="col-md-3">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                {!! Form::open(['url' => 'user/store', 'method' =>'post']) !!}
                <div class="form-group">
                    {!! Form::label('Name') !!}
                    {!! Form::text('name', null, ['class'=> 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Email') !!}
                    {!! Form::text('email', null, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('password') !!}
                    {!! Form::password('password', null, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Bio') !!}
                    {!! Form::textarea('bio', null, ['class'=> 'form-control']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('Facebook') !!}
                    {!! Form::url('facebook', null, ['class'=> 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Twitter') !!}
                    {!! Form::url('twitter', null, ['class'=> 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Github') !!}
                    {!! Form::url('github', null, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Submit', null, ['class'=> 'form-control']) !!}
                </div>


                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection