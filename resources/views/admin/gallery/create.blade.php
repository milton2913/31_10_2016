@extends('layouts.app')

@section('content')
    <div class="col-md-3">
    </div>
    <div class="container">

        <div class="row">
            <div class="col-md-4">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['url' => 'profession', 'method' =>'post']) !!}
                <div class="form-group">
                    {!! Form::label('Name') !!}
                    {!! Form::text('name', null, ['class'=> 'form-control']) !!}
                </div>


                <div class="form-group">
                    {!! Form::submit('Submit', null, ['class'=> 'form-control']) !!}
                </div>


                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection