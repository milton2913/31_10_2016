@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="{{url('/profession/create')}}">New Profession</a>
                @if (Session::has('message'))
                    <div class="alert alert-success "><i class="close"></i>
                        {{ Session::get('message') }}
                    </div>
                @endif
                <div class="panel panel-default">



                    <table border="1" class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Id</td>
                            <td>Name</td>

                            <td>Action</td>

                        </tr>
                        @foreach($professions as $profession)
                            <tr>
                                <td>{{$profession->id}}</td>
                                <td>{{$profession->name}}</td>

                                <td><a href="/profession/{{$profession->id.'/edit'}}">Edit</a>|<a href="/profession/{{$profession->id}}">Details</a>

                                    {!! Form::open(['url' => 'profession/'.$profession->id, 'method' =>'DELETE']) !!}
                                    <div class="form-group">
                                        {!! Form::submit('delete', null, ['class'=> 'form-control']) !!}
                                    </div>
                                    {!! Form::close() !!}

                                </td>
                            </tr>
                        @endforeach

                        </thead>

                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
