@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    @if (Session::has('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div>
                    @endif

                    <table border="1" class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Id</td>
                            <td>Name</td>
                            <td>Create Time</td>
                        </tr>
                            <tr>
                                <td>{{$profession->id}}</td>
                                <td>{{$profession->name}}</td>
                                <td>{{$profession->created_at}}</td>
                            </tr>
                        </thead>

                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
